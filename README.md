**How to run**

1. Simple `yarn install` and `yarn ios` or `yarn android` should do the trick.

**Project structure**

Key components
- `Redux` is being used for state manager
- `react-navigation` is being used for navigation
- `expo-local-authentication` for authorization. (Currently, only tested with Android due to physical device limitation)


`src` - TS codebase resides here.
    - `assets` - contains constant assets, theme data, static images etc...
    - `components` - contains individual components...
    - `navigator` - contains all the necessary navigation configurations and setup ( this case - react-navigation)
    - `pages` - contains all the pages 
    - `states` - contains all defined reducers
    - `store` - only contains the configuration file of the store
    - `types` - contains all the necessary types for the app
    - `utility` - contains all the defined utility functions.
