import React from 'react';
import renderer from 'react-test-renderer';

import App from '../src/App';

describe('<App />', () => {
  it('has 1 child', () => {
    const tree = renderer.create(<App />);
    expect(tree.toTree().children.length).toBe(1);
  });
});