module.exports = {
  presets: ['module:metro-react-native-babel-preset'],
  plugins: [
    [
      require.resolve('babel-plugin-module-resolver'),
      {
        root: ['./src'],
        cwd: 'babelrc',
        extensions: ['.ts', '.tsx', '.js', '.ios.js', '.android.js'],
        alias: {
          '@components': 'src/components',
          '@types': 'src/types',
          '@pages': 'src/pages',
          '@store': 'src/store',
          '@states': 'src/states',
          '@reducer': 'src/reducer',
          '@tests': 'src/tests',
          '@utility': 'src/utility',
        },
      },
    ],
    'jest-hoist',
  ],
};
