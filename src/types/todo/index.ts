export type Todo = {
  id: number;
  title: string;
  description: string;
  status: boolean;
  subtasks?: SubTask[];
  date?: string | Date;
  createdAt: string | Date;
  updatedAt?: string | Date;
};

export type SubTask = {
  title: string;
  status: boolean;
};

export type TodoGroup = {
  id: number;
  title: 'Business' | 'Personal';
};
