import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

const Stack = createNativeStackNavigator();

// Pages
import {TodoListPage} from '../../pages/todo/todoList';
import {TodoPage} from 'pages/todo/todo';
import {TodoForm} from 'pages/todo/form';
import {AuthenticationPage} from 'pages/authentication';

export const StackRoutes = {
  auth: 'auth',
  todoList: 'todoList',
  todo: 'todo',
  todoForm: 'todoForm',
};

export const StackNavigator = () => (
  <Stack.Navigator
    initialRouteName={StackRoutes.auth}
    screenOptions={{
      headerShown: false,
    }}>
    <Stack.Screen name={StackRoutes.auth} component={AuthenticationPage} />
    <Stack.Screen name={StackRoutes.todoList} component={TodoListPage} />
    <Stack.Screen name={StackRoutes.todo} component={TodoPage} />
    <Stack.Screen name={StackRoutes.todoForm} component={TodoForm} />
  </Stack.Navigator>
);
