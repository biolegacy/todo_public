import {createSlice, PayloadAction} from '@reduxjs/toolkit';
import {RootState} from '../../store';
import {Todo} from '../../types/todo';
import {TodoState, UpdateSubtaskStatusPayload} from './type';

const initialState: TodoState = {
  todos: [
    {
      id: 1,
      title: 'Todo 1',
      description: 'Todo 1 description',
      status: false,
      date: '',
      createdAt: '',
      updatedAt: '',
    } as Todo,
    {
      id: 2,
      title: 'Todo 2',
      description: 'Todo 3 description',
      status: false,
      subtasks: [
        {
          title: 'SubTask 1',
          status: false,
        },
        {
          title: 'SubTask 2',
          status: true,
        },
        {
          title: 'SubTask 3',
          status: false,
        },
      ],
      date: '',
      createdAt: '',
      updatedAt: '',
    } as Todo,
    {
      id: 3,
      title: 'Todo 3',
      description: 'Todo 3 description',
      status: true,
      date: '',
      createdAt: '',
      updatedAt: '',
    } as Todo,
  ],
  latestId: 3,
};

export const TodoSlice = createSlice({
  name: 'todo-state',
  initialState,
  reducers: {
    updateTodos: (state: TodoState, action: PayloadAction<Todo[]>) => {
      state.todos = action.payload;
    },
    clearTodos: (state: TodoState) => {
      state.todos = undefined;
    },
    selectTodo: (state: TodoState, action: PayloadAction<Todo>) => {
      state.todo = action.payload;
    },
    clearTodo: (state: TodoState) => {
      state.todo = undefined;
    },
    updateTodo: (state: TodoState, action: PayloadAction<Todo>) => {
      if (state.todos && state.todos.length > 0) {
        const oldTodo = state.todos.find(item => item.id === action.payload.id);
        state.todos.splice(state.todos.indexOf(oldTodo), 1, action.payload);
      }
    },
    updateTodoStatus: (state: TodoState, action: PayloadAction<number>) => {
      if (state.todos && state.todos.length > 0) {
        state.todos = state.todos.map(item => {
          if (item.id === action.payload) {
            item.status = !item.status;
            state.todo = item;
          }
          return item;
        });
      }
    },
    updateSubtaskStatus: (
      state: TodoState,
      action: PayloadAction<UpdateSubtaskStatusPayload>,
    ) => {
      // TODO: Refactor this function's logic
      const {todoId, index} = action.payload;
      const updatedTodo = state.todos?.find(item => item.id === todoId);
      // if Todo with todoId doesn't exist don't proceed further
      if (!updatedTodo || !state.todos || state.todos.length === 0) {
        return;
      }

      // Since we know the todo with todoId exist in the todos, we can safely cast as number
      const todoIndex: number = state.todos?.indexOf(updatedTodo) as number;

      if (
        updatedTodo &&
        updatedTodo.subtasks &&
        updatedTodo.subtasks.length > 0
      ) {
        updatedTodo.subtasks[index].status =
          !updatedTodo.subtasks[index].status;

        const updatedTodos = state.todos;
        updatedTodos.splice(todoIndex, 1, updatedTodo);

        state.todos = [...updatedTodos];
      }
      // update selected todo states
      if (state.todo && state.todo.subtasks) {
        state.todo.subtasks[index].status = !state.todo.subtasks[index].status;
      }
    },
    deleteTodo: (state: TodoState, action: PayloadAction<number>) => {
      const element = state.todos?.find(item => item.id === action.payload);
      if (!element) {
        return;
      }

      const index: number = state.todos?.indexOf(element) as number;

      if (index !== -1 && state.todos && state.todos.length !== 0) {
        if (state.todos.length === 1) {
          state.todos = undefined;
        }
        state.todos?.splice(index, 1);
      }
    },
    addTodo: (state: TodoState, action: PayloadAction<Todo>) => {
      const updatedTodos = state.todos ? state.todos : ([] as Todo[]);
      updatedTodos.push(action.payload);
      state.todos = [...updatedTodos];
      state.latestId++;
    },
  },
});

export const {
  updateTodos,
  clearTodos,
  selectTodo,
  clearTodo,
  updateTodo,
  updateTodoStatus,
  updateSubtaskStatus,
  deleteTodo,
  addTodo,
} = TodoSlice.actions;

export const getTodoState = (state: RootState) => state.todoReducer;
