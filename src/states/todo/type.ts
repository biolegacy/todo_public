import {Todo} from '../../types/todo';

export type TodoState = {
  todos?: Todo[];
  todo?: Todo;
  latestId: number;
};

export type UpdateSubtaskStatusPayload = {
  todoId: number;
  index: number;
};
