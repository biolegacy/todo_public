import React from 'react';
import {Provider} from 'react-redux';
import {store} from './store';
import {Navigator} from './navigator';

// UI Library import and its constants
import * as eva from '@eva-design/eva';
import {ApplicationProvider, IconRegistry} from '@ui-kitten/components';
import {ThemeContext} from './utility/theme/toggler';
import {default as baseTheme} from 'assets/theme_data/custom-theme.json';
import {EvaIconsPack} from '@ui-kitten/eva-icons';

const RootApp = () => {
  const [theme, setTheme] = React.useState('light');

  const toggleTheme = () => {
    const nextTheme = theme === 'light' ? 'dark' : 'light';
    setTheme(nextTheme);
  };

  return (
    <Provider store={store}>
      <IconRegistry icons={EvaIconsPack} />
      <ThemeContext.Provider value={{theme, toggleTheme}}>
        <ApplicationProvider {...eva} theme={{...eva[theme], ...baseTheme}}>
          <Navigator />
        </ApplicationProvider>
      </ThemeContext.Provider>
    </Provider>
  );
};

export default RootApp;
