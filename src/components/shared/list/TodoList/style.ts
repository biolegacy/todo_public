import {StyleSheet} from 'react-native';

export const style = StyleSheet.create({
  container: {},
  buttonWrapper: {
    marginVertical: 8,
    paddingVertical: 12,
    paddingHorizontal: 16,
    paddingRight: 8,
    padding: 0,
    borderWidth: 1,
    borderStyle: 'solid',
    borderColor: '#eaeaea',
    borderRadius: 4,
    alignItems: 'center',
    justifyContent: 'flex-start',
    flexDirection: 'row',
  },
  button: {
    flexGrow: 1,
    backgroundColor: '#FFFFFF00',
  },
  buttonDescriptionWrapper: {
    width: '100%',
    flexDirection: 'column',
    backgroundColor: '#FFFFFF00',
  },
  buttonTitle: {
    fontSize: 16,
  },
  buttonDescription: {
    marginTop: 8,
    fontSize: 12,
    fontWeight: '300',
  },
  subTasksContainer: {
    marginHorizontal: 10,
    alignSelf: 'flex-end',
    backgroundColor: '#FFFFFF00',
  },
  controlsContainer: {
    backgroundColor: '#FFFFFF00',
    alignSelf: 'stretch',
    flexDirection: 'row',
    alignItems: 'center',
  },
  control: {
    padding: 8,
  },
  controlIcon: {
    width: 24,
    height: 24,
  },
});
