import {useNavigation} from '@react-navigation/native';
import {NativeStackNavigationProp} from '@react-navigation/native-stack';
import {Icon, Layout, Text, useTheme} from '@ui-kitten/components';
import {StackRoutes} from 'navigator/stack';
import React, {FC} from 'react';
import {FlatList, TouchableOpacity} from 'react-native';
import {useDispatch} from 'react-redux';
import {deleteTodo, selectTodo} from 'states/todo';
import {Todo} from 'types/todo';
import {getCompletedSubtasksCount, isSubTasksDone} from 'utility/functions';

// Importing style
import {style} from './style';

interface FlatBasedListProps {
  data: Todo[];
}

export const TodoList: FC<FlatBasedListProps> = ({data}) => {
  const navigation = useNavigation<NativeStackNavigationProp<any>>();
  const dispatch = useDispatch();
  const theme = useTheme();

  /**
   * Logic of the on press
   */
  const handleOnClick = (item: Todo): void => {
    dispatch(selectTodo(item));
    navigation.navigate(StackRoutes.todo, {todo: item});
  };

  /**
   *
   * @param id Todo item
   */
  const deleteTodoRequest = (id: number) => {
    // Todo: Delete todo logic
    dispatch(deleteTodo(id));
  };

  /**
   *
   * @param item
   */
  const editTodoRequest = (data: Todo) => {
    // TODO: Navigate to Todo Form
    navigation.navigate(StackRoutes.todoForm, {data, isEdit: true});
  };

  /**
   *
   * @param item Todo item
   * @returns color code
   */
  const getTodoBorderColor = (item: Todo): string => {
    if (item.subtasks) {
      return isSubTasksDone(item)
        ? theme['color-success-600']
        : theme['border-basic-color-4'];
    }
    return item.status
      ? theme['color-success-600']
      : theme['border-basic-color-4'];
  };

  /**
   *
   * @param item Todo item
   * @returns color code
   */
  const getTodoBgColor = (item: Todo): string => {
    if (item.subtasks) {
      return isSubTasksDone(item)
        ? theme['color-success-transparent-100']
        : theme['color-basic-100'];
    }
    return item.status
      ? theme['color-success-transparent-100']
      : theme['color-basic-100'];
  };

  /**
   *
   * @param item Todo item
   */
  const renderItem = (item: Todo) => {
    return (
      <TouchableOpacity key={item.id} onPress={() => handleOnClick(item)}>
        <Layout
          style={{
            ...style.buttonWrapper,
            borderColor: getTodoBorderColor(item),
            backgroundColor: getTodoBgColor(item),
          }}>
          <Layout style={style.button}>
            <Layout style={style.buttonDescriptionWrapper}>
              <Text style={style.buttonTitle}>{item.title}</Text>
              <Text style={style.buttonDescription}>{item.description}</Text>
            </Layout>
          </Layout>
          {item.subtasks && (
            <Layout style={style.subTasksContainer}>
              <Text
                style={{
                  color: isSubTasksDone(item)
                    ? theme['color-success-600']
                    : theme['text-basic-color'],
                }}>
                {getCompletedSubtasksCount(item)} / {item.subtasks.length}{' '}
                {item.subtasks.length > 1 ? 'Tasks' : 'Task'}
              </Text>
            </Layout>
          )}
          <Layout style={style.controlsContainer}>
            <TouchableOpacity
              style={style.control}
              onPress={() => editTodoRequest(item)}>
              <Icon
                style={style.controlIcon}
                fill={theme['border-basic-color-5']}
                name="settings-outline"
              />
            </TouchableOpacity>
            <TouchableOpacity
              style={style.control}
              onPress={() => deleteTodoRequest(item.id)}>
              <Icon
                style={style.controlIcon}
                fill={theme['color-danger-400']}
                name="trash-2-outline"
              />
            </TouchableOpacity>
          </Layout>
        </Layout>
      </TouchableOpacity>
    );
  };

  return <FlatList data={data} renderItem={({item}) => renderItem(item)} />;
};
