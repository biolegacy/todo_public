import {StyleSheet} from 'react-native';

export const style = StyleSheet.create({
  container: {
    paddingVertical: 12,
    paddingHorizontal: 8,
    borderWidth: 1,
    borderLeftWidth: 3,
    borderRadius: 6,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  subTasksHeaderIcon: {
    width: 24,
    height: 24,
  },
});
