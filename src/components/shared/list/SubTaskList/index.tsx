import {Icon, Text, useTheme} from '@ui-kitten/components';
import React, {FC} from 'react';
import {FlatList, TouchableOpacity} from 'react-native';
import {useDispatch} from 'react-redux';
import {updateSubtaskStatus} from 'states/todo';
import {SubTask} from 'types/todo';

// Importing style
import {style} from './style';

interface SubTaskListProps {
  data: SubTask[];
  todoId: number;
}

export const SubTaskList: FC<SubTaskListProps> = ({data, todoId}) => {
  const theme = useTheme();
  const dispatch = useDispatch();

  /**
   *
   * @param index of the subtask of TODO
   */
  const updateSubtaskStatusLocal = (index: number) => {
    dispatch(
      updateSubtaskStatus({
        todoId,
        index,
      }),
    );
  };

  /**
   *
   * @param item Todo Subtask
   * @param index index of the subtask
   * @returns Subtask render
   */
  const renderItem = (item: SubTask, index: number) => {
    return (
      <TouchableOpacity
        onPress={() => updateSubtaskStatusLocal(index)}
        style={{
          ...style.container,
          marginTop: index === 0 ? 0 : 8,
          borderColor: theme['border-basic-color-4'],
          borderLeftColor: item.status
            ? theme['color-success-default']
            : theme['border-basic-color-5'],
        }}>
        <Text>{item.title}</Text>
        <Icon
          style={style.subTasksHeaderIcon}
          fill={
            item.status
              ? theme['color-success-default']
              : theme['border-basic-color-5']
          }
          name="checkmark-circle-outline"
        />
      </TouchableOpacity>
    );
  };

  return (
    <FlatList
      data={data}
      renderItem={({item, index}) => renderItem(item, index)}
    />
  );
};
