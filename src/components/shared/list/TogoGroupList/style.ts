import {StyleSheet} from 'react-native';

export const style = StyleSheet.create({
  container: {
    marginTop: 25,
    flexDirection: 'row',
    justifyContent: 'space-evenly',
  },
  buttonImageWrapper: {
    ...StyleSheet.absoluteFillObject,
  },
  button: {
    width: 150,
    height: 150,
    borderRadius: 4,
    borderWidth: 1,
    borderColor: '#eaeaea',
    backgroundColor: '#fff',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 12,
  },
});
