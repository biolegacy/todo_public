import {Layout, Text} from '@ui-kitten/components';
import React from 'react';
import {TouchableOpacity, View} from 'react-native';
import {format} from 'date-fns';

// Importing style
import {style} from './style';

export const TodoGroupList = () => {
  const today = new Date();

  return (
    <Layout style={style.container}>
      <TouchableOpacity style={style.button}>
        <View style={style.buttonImageWrapper} />
        <Text category='p1' style={{fontWeight: '700'}}>{format(today, 'EEEE')}</Text>
        <Text category='h1'>{today.getDate()}</Text>
        <Text category='p1'>{format(today, 'LLLL')}</Text>
      </TouchableOpacity>
      <TouchableOpacity style={style.button}>
        <View style={style.buttonImageWrapper} />
        <Text style={{fontWeight: '700'}}>Progress</Text>
        <Text category='h1'>75%</Text>
        <Text />
      </TouchableOpacity>
    </Layout>
  );
};
