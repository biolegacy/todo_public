import {useNavigation} from '@react-navigation/native';
import {NativeStackNavigationProp} from '@react-navigation/native-stack';
import {Icon, Layout, Text, useTheme} from '@ui-kitten/components';
import {StackRoutes} from 'navigator/stack';
import React, {FC} from 'react';
import {TouchableOpacity} from 'react-native';

// Importing style
import {style} from './style';

interface HeaderProps {
  title: string;
}

export const Header: FC<HeaderProps> = ({title}) => {
  const navigation = useNavigation<NativeStackNavigationProp<any>>();
  const theme = useTheme();

  /**
   * @description Navigate user to the todo form
   */
  const navigateToForm = () => {
    navigation.navigate(StackRoutes.todoForm);
  };

  return (
    <Layout style={style.container}>
      <Layout>
        <Text>{title}</Text>
      </Layout>
      <TouchableOpacity onPress={navigateToForm}>
        <Icon
          style={style.controlIcon}
          fill={theme['text-basic-color']}
          name="plus-outline"
        />
      </TouchableOpacity>
    </Layout>
  );
};
