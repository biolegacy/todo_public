import {Dimensions, Platform} from 'react-native';

export const {width: screen_width, height: screen_height} =
  Dimensions.get('window');

export const PaddingTop = Platform.OS === 'ios' ? 50 : 0;
