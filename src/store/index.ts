import {configureStore} from '@reduxjs/toolkit';
import {TodoSlice} from '../states/todo';

export const store = configureStore({
  reducer: {
    todoReducer: TodoSlice.reducer,
  },
});

export type RootState = ReturnType<typeof store.getState>;

export type AppDispatch = typeof store.dispatch;
