import {Todo} from 'types/todo';

export const isSubTasksDone = (data: Todo) => {
  if (!data.subtasks || data.subtasks.length === 0) {
    return true;
  }
  let result = true;
  data.subtasks.forEach(task => {
    if (!task.status) {
      result = false;
    }
  });
  return result;
};

/**
 *
 * @param todo Todo
 * @returns count of the completed subtasks
 */
export const getCompletedSubtasksCount = (todo: Todo): number => {
  let count = 0;
  todo?.subtasks?.forEach(item => {
    if (item.status) {
      count++;
    }
  });
  return count;
};
