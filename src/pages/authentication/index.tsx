import React, {useCallback, useEffect} from 'react';
import * as LocalAuthentication from 'expo-local-authentication';
import {Alert, Text, View} from 'react-native';
import Constants from 'expo-constants';
import {useNavigation} from '@react-navigation/native';
import {StackRoutes} from 'navigator/stack';
import {NativeStackNavigationProp} from '@react-navigation/native-stack';

export const AuthenticationPage = () => {
  const navigation = useNavigation<NativeStackNavigationProp<any>>();
  // wherever the useState is located
  const [isBiometricSupported, setIsBiometricSupported] = React.useState(false);

  const checkBiometicSupport = async () => {
    console.log('LocalAuthentication', LocalAuthentication);
    const {hasHardwareAsync} = LocalAuthentication;
    const compatible = await hasHardwareAsync();
    setIsBiometricSupported(compatible);
  };

  const handleBiometricAuth = useCallback(async () => {
    const savedBiometrics = await LocalAuthentication.isEnrolledAsync();
    console.log('Saved biometrics? ', savedBiometrics);
    if (!savedBiometrics) {
      Alert.alert(
        'Biometric record not found',
        'Please verify your identity with your password',
        [
          {
            text: 'Ask me later',
            onPress: () => console.log('Ask me later pressed'),
          },
          {
            text: 'Cancel',
            onPress: () => console.log('Cancel Pressed'),
            style: 'cancel',
          },
          {text: 'OK', onPress: () => console.log('OK Pressed')},
        ],
      );
    }

    const result = await LocalAuthentication.authenticateAsync();
    if (result.success) {
      navigation.navigate(StackRoutes.todoList);
    }
  }, [isBiometricSupported]);

  // Check if hardware supports biometrics
  useEffect(() => {
    checkBiometicSupport();
  });

  useEffect(() => {
    handleBiometricAuth();
  }, [handleBiometricAuth]);

  return (
    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
      <Text>123</Text>
      <Text>
        {isBiometricSupported
          ? 'Your device is compatible with Biometrics'
          : 'Face or Fingerprint scanner is available on this device'}
      </Text>
    </View>
  );
};
