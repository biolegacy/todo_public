import {useNavigation, useRoute} from '@react-navigation/native';
import {
  Divider,
  Layout,
  Text,
  Input,
  useTheme,
  Icon,
  Button,
} from '@ui-kitten/components';
import React, {FC, useState} from 'react';
import {useEffect} from 'react';
import {ScrollView, TouchableOpacity} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {addTodo, updateTodo} from 'states/todo';
import {RootState} from 'store';
import {SubTask, Todo} from 'types/todo';
import {style} from './style';

interface TodoFormProps {}

const useInputState = (initialValue = '') => {
  const [value, setValue] = React.useState(initialValue);
  return {value, onChangeText: setValue};
};

export const TodoForm: FC<TodoFormProps> = () => {
  const {latestId} = useSelector((state: RootState) => state.todoReducer);
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const theme = useTheme();
  const {data, isEdit} = useRoute().params as {data: Todo; isEdit: boolean};

  console.log('DATA: ', data);
  // States
  const titleInputState = useInputState(data?.title);
  const descriptionInputstate = useInputState(data?.description);
  const [subtasks, setSubtasks] = useState<SubTask[]>(data.subtasks || []);

  /**
   * @returns void
   */
  const goBack = () => {
    navigation.goBack();
  };

  /**
   * @description Create Todo from the form
   */
  const submitTodo = () => {
    if (isEdit) {
      const updatedTodo: Todo = {
        ...data,
        title: titleInputState.value,
        description: descriptionInputstate.value,
        subtasks: subtasks.length > 0 ? subtasks : undefined,
        updatedAt: new Date().toString(),
      };
      dispatch(updateTodo(updatedTodo));
    } else {
      const newTodo: Todo = {
        id: latestId + 1,
        title: titleInputState.value,
        description: descriptionInputstate.value,
        subtasks: subtasks.length > 0 ? subtasks : undefined,
        status: false,
        createdAt: new Date().toString(),
      };
      dispatch(addTodo(newTodo));
    }
    goBack();
  };

  /**
   * @description add subtask to Todo
   */
  const addSubTask = () => {
    const newSubTask: SubTask = {
      title: '',
      status: false,
    };
    subtasks.push(newSubTask);
    setSubtasks([...subtasks]);
  };

  /**
   *
   * @param index index of the subtask
   */
  const removeSubtask = (index: number) => {
    const newList = subtasks;
    newList.splice(index, 1);
    setSubtasks([...newList]);
  };

  /**
   *
   * @param value New title value
   * @param index of the subTask
   */
  const changeSubtaskTitle = (value: string, index: number) => {
    subtasks[index].title = value;
    setSubtasks([...subtasks]);
  };

  /**
   *
   * @param index of the subtask
   * @returns title input of the subtasl
   */
  const renderSubTaskInput = (index: number) => {
    return (
      <Layout key={`subtask_${index}`} style={style.subtaskContainer}>
        <Layout style={style.subtaskInputContainer}>
          <Input
            placeholder="New sub task"
            value={subtasks[index].title}
            onChangeText={value => changeSubtaskTitle(value, index)}
          />
        </Layout>
        <TouchableOpacity
          style={{
            ...style.subtaskDeleteButton,
            borderColor: theme['color-control-focus-border'],
          }}
          onPress={() => removeSubtask(index)}>
          <Icon
            style={style.subtaskDeleteIcon}
            fill={theme['color-control-focus-border']}
            name="close-outline"
          />
        </TouchableOpacity>
      </Layout>
    );
  };

  /**
   *
   * @returns SubTasks options
   */
  const renderSubTasksOptions = () => {
    if (subtasks.length === 0) {
      return undefined;
    }
    return (
      <Layout>
        <Text category="h6">Subtasks</Text>
        <Divider style={{marginVertical: 8}} />
        {subtasks.map((_, index) => renderSubTaskInput(index))}
      </Layout>
    );
  };

  return (
    <Layout style={style.container}>
      <Layout style={style.header}>
        <TouchableOpacity
          onPress={goBack}
          style={{
            ...style.backButton,
            borderColor: theme['text-basic-color'],
          }}>
          <Icon
            style={style.backButtonIcon}
            fill={theme['text-basic-color']}
            name="arrow-left-outline"
          />
        </TouchableOpacity>
        <Text category="h3">Todo Form</Text>
        <TouchableOpacity
          onPress={submitTodo}
          style={{
            ...style.backButton,
            borderColor: theme['text-basic-color'],
          }}>
          <Icon
            style={style.backButtonIcon}
            fill={theme['text-basic-color']}
            name="checkmark-outline"
          />
        </TouchableOpacity>
      </Layout>
      <ScrollView contentContainerStyle={style.scrollview}>
        <Divider style={{marginBottom: 12}} />
        <Layout style={style.formRow}>
          <Text category="s1" style={style.formLabel}>
            Title
          </Text>
          <Input placeholder="Todo title" {...titleInputState} />
        </Layout>
        <Layout style={style.formRow}>
          <Text category="s1" style={style.formLabel}>
            Description
          </Text>
          <Input
            placeholder="Todo description"
            multiline
            numberOfLines={4}
            textStyle={{height: 80}}
            {...descriptionInputstate}
          />
        </Layout>
        <Layout style={style.formRow}>
          {renderSubTasksOptions()}
          <Button onPress={addSubTask} style={{marginTop: 20}}>
            <Text>Add subtask</Text>
          </Button>
        </Layout>
      </ScrollView>
    </Layout>
  );
};
