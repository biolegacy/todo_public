import {PaddingTop} from 'components/shared/style';
import {StyleSheet} from 'react-native';

export const style = StyleSheet.create({
  container: {
    paddingTop: PaddingTop,
    paddingHorizontal: 16,
    flex: 1,
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: 6,
  },
  backButton: {
    width: 28,
    height: 28,
    borderRadius: 18,
    borderColor: '#6a6a6a',
    borderWidth: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  backButtonIcon: {
    width: 22,
    height: 22,
  },
  scrollview: {
    flexGrow: 1,
    backgroundColor: '#FFFFFF00',
  },
  formRow: {
    paddingVertical: 6,
  },
  formLabel: {
    fontSize: 16,
    marginBottom: 8,
  },
  subtaskContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 8,
  },
  subtaskInputContainer: {
    flexGrow: 1,
    marginRight: 10,
  },
  subtaskDeleteButton: {
    width: 26,
    height: 26,
    borderRadius: 18,
    borderWidth: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  subtaskDeleteIcon: {
    width: 18,
    height: 18,
  },
});
