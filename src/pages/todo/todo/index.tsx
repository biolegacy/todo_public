import {useNavigation} from '@react-navigation/native';
import {Divider, Icon, Layout, Text, useTheme} from '@ui-kitten/components';
import {SubTaskList} from 'components/shared/list/SubTaskList';
import React, {FC} from 'react';
import {TouchableOpacity} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {clearTodo, updateTodoStatus} from 'states/todo';
import {RootState} from 'store';
import {getCompletedSubtasksCount, isSubTasksDone} from 'utility/functions';

// Importing style
import {style} from './style';

interface TodoPageProps {}

export const TodoPage: FC<TodoPageProps> = () => {
  const {todo} = useSelector((state: RootState) => state.todoReducer);
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const theme = useTheme();

  /**
   * @returns void
   */
  const goBack = () => {
    dispatch(clearTodo());
    navigation.goBack();
  };

  /**
   *
   * @param id of Todo
   */
  const updateTodoStatusRequest = (id?: number) => {
    if (!id) {
      return;
    }
    dispatch(updateTodoStatus(id));
  };

  const renderTodoCompleteButton = () => {
    if (todo?.subtasks && todo.subtasks.length > 0) {
      return undefined;
    }
    return (
      <Layout
        style={{
          ...style.completedButton,
        }}>
        <TouchableOpacity onPress={() => updateTodoStatusRequest(todo?.id)}>
          <Layout style={style.completedButtonContent}>
            <Text
              category="p1"
              style={{
                ...style.completedButtonContentText,
                color: todo?.status
                  ? theme['color-success-default']
                  : theme['text-basic-color'],
              }}>
              Complete
            </Text>
            <Icon
              style={style.backButtonIcon}
              fill={
                todo?.status
                  ? theme['color-success-default']
                  : theme['text-basic-color']
              }
              name="checkmark-square-outline"
            />
          </Layout>
        </TouchableOpacity>
      </Layout>
    );
  };

  /**
   *
   * @returns Subtasks
   */
  const renderSubTasks = () => {
    if (!todo?.subtasks || todo.subtasks.length === 0) {
      return (
        <Layout style={[style.subTasksContainer, style.noSubTasksContainer]}>
          <Layout style={style.noSubtasksTextWrapper}>
            <Text category="h5" style={style.noSubtasksText}>
              Currently, there is no subtask
            </Text>
          </Layout>
        </Layout>
      );
    }
    return (
      <Layout style={style.subTasksContainer}>
        <Layout style={style.subTasksHeader}>
          <Icon
            style={style.subTasksHeaderIcon}
            fill={
              isSubTasksDone(todo)
                ? theme['color-success-default']
                : theme['border-basic-color-5']
            }
            name="checkmark-circle-outline"
          />
          <Text category="h6">
            {getCompletedSubtasksCount(todo)}/{todo.subtasks.length}
          </Text>
        </Layout>
        <Layout style={style.subTasksListWrapper}>
          <SubTaskList data={todo.subtasks} todoId={todo.id} />
        </Layout>
      </Layout>
    );
  };

  return (
    <Layout style={style.container}>
      <Layout style={style.header}>
        <Layout style={style.backWrapper}>
          <TouchableOpacity
            onPress={goBack}
            style={{
              ...style.backButton,
              borderColor: theme['text-basic-color'],
            }}>
            <Icon
              style={style.backButtonIcon}
              fill={theme['text-basic-color']}
              name="arrow-left-outline"
            />
          </TouchableOpacity>
          {renderTodoCompleteButton()}
        </Layout>
        <Text category="h1" style={style.headerTitle}>
          {todo?.title}
        </Text>
        <Text style={style.headerDescription}>{todo?.description}</Text>
      </Layout>
      <Divider />
      <Layout>{renderSubTasks()}</Layout>
    </Layout>
  );
};
