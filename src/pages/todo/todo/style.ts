import {PaddingTop, screen_height} from 'components/shared/style';
import {Platform, StyleSheet} from 'react-native';

const header_height = 280;

export const style = StyleSheet.create({
  container: {
    flex: 1,
    display: 'flex',
  },
  header: {
    paddingTop: PaddingTop,
    height: header_height,
    justifyContent: 'center',
    alignItems: 'center',
  },
  headerTitle: {},
  headerDescription: {
    maxWidth: '80%',
    width: '80%',
    textAlign: 'center',
  },
  backWrapper: {
    ...StyleSheet.absoluteFillObject,
    paddingTop: Platform.OS === 'ios' ? 50 : 15,
    justifyContent: 'space-between',
    alignItems: 'flex-start',
  },
  backButton: {
    marginLeft: 15,
    width: 36,
    height: 36,
    borderRadius: 18,
    borderColor: '#6a6a6a',
    borderWidth: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  backButtonIcon: {
    width: 24,
    height: 24,
  },
  completedButton: {
    alignSelf: 'stretch',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 16,
  },
  completedButtonContent: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  completedButtonContentText: {
    fontSize: 18,
    marginRight: 6,
  },
  subTasksContainer: {
    height: screen_height - header_height,
    flexDirection: 'column',
  },
  subTasksHeader: {
    padding: 12,
    paddingHorizontal: 18,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  subTasksListWrapper: {
    paddingHorizontal: 24,
    paddingVertical: 16,
    flexGrow: 1,
  },
  subTasksHeaderIcon: {
    width: 18,
    height: 18,
    marginRight: 12,
  },
  noSubTasksContainer: {
    justifyContent: 'flex-start',
    paddingTop: 175,
    alignItems: 'center',
  },
  noSubtasksTextWrapper: {
    flexDirection: 'row',
    maxWidth: '75%',
  },
  noSubtasksText: {
    textAlign: 'center',
    fontWeight: '300',
  },
});
