import {Divider, Layout} from '@ui-kitten/components';
import {TodoList} from 'components/shared/list';
import {TodoGroupList} from 'components/shared/list/TogoGroupList';
import React, {FC} from 'react';
import {useSelector} from 'react-redux';
import {RootState} from 'store';
import {Header} from '../../../components/shared/header';

// Importing style
import {style} from './style';

interface TodoListPage {}

export const TodoListPage: FC<TodoListPage> = () => {
  const {todos} = useSelector((state: RootState) => state.todoReducer);
  return (
    <Layout style={{...style.container}}>
      <Header title="Jimmy Conner" />
      <Divider />
      <TodoGroupList />
      <Layout style={style.listContainer}>
        {todos && <TodoList data={todos} />}
      </Layout>
    </Layout>
  );
};
