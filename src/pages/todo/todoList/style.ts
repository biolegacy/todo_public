import {StyleSheet} from 'react-native';
import {PaddingTop, screen_height} from '../../../components/shared/style';

export const style = StyleSheet.create({
  container: {
    paddingTop: PaddingTop,
    flex: 1,
    flexGrow: 1,
    minHeight: screen_height,
  },
  listContainer: {
    padding: 16,
    flexDirection: 'column',
    flexGrow: 1,
  },
});
